//验证密码必须有字母、数字、特殊字符中的两种
import axios from "axios";
import {clearNickName, clearToken} from "@/utils/setToken";



//判断是否为手机号的正则表达式


function setCookie(cname,cvalue,exdays){
    var d = new Date();
    d.setTime(d.getTime()+(exdays*24*60*60*1000));
    var expires = "expires="+d.toGMTString();
    document.cookie = cname+"="+cvalue+"; "+expires+";domain=.wncinema.com;path=/";
}




//设置token
const TokenKey = 'token'

export default function getToken() {
    return localStorage.getItem('token')
}

export function setToken(token) {
  return setCookie(TokenKey, token, 1000)
}

function removeToken() {
  return setCookie(TokenKey, null, 0)
}

//获取url中的参数


//设置axios拦截request和response
axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'
axios.defaults.timeout=10000
axios.defaults.baseURL = 'http://127.0.0.1:9000/'
const axio = axios.create({
  baseURL: 'http://127.0.0.1:9000/',
  timeout: 10000
})

// request拦截器
axios.interceptors.request.use(config => {
  // 是否需要设置 token

  const isToken = (config.headers || {}).isToken === false
  if (getToken() && !isToken) {
    config.headers['Authorization'] = 'Bearer ' + getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
  }
  // get请求映射params参数
  if (config.method === 'get' && config.params) {
    let url = config.url + '?';
    for (const propName of Object.keys(config.params)) {
      const value = config.params[propName];
      var part = encodeURIComponent(propName) + "=";
      if (value !== null && typeof(value) !== "undefined") {
        if (typeof value === 'object') {
          for (const key of Object.keys(value)) {
            let params = propName + '[' + key + ']';
            var subPart = encodeURIComponent(params) + "=";
            url += subPart + encodeURIComponent(value[key]) + "&";
          }
        } else {
          url += part + encodeURIComponent(value) + "&";
        }
      }
    }
    url = url.slice(0, -1);
    config.params = {};
    config.url = url;
  }
  return config
}, error => {
    // alert(error)
    console.log(error)
    Promise.reject(error)
})

// 响应拦截器
axios.interceptors.response.use(res => {
    // 未设置状态码则默认成功状态
    const code = res.data.code || 200;
    // 获取错误信息
    const msg = res.data.msg

	console.log('response-->',res)
    if (code === 401) {
		alert('请登录')
		location.href = '../login.html';

    } else if (code === 500) {
      alert('后台服务异常')
      return Promise.reject(""+msg)
    }  else if (code === 9012 || code ===9014) {
        clearToken();
        clearNickName();
        alert('请登录')
        location.href = '../login.html';
    } else if(code === 200) {
        return res.data
    } else {
        if (msg === ""){
            return res.data
        }
        alert(msg)
        return res.data
    }

  },
  error => {
    console.log(error)
      let msg = error.message
      if (msg.indexOf("401")>=0){
          alert("请登录！")
          removeToken()
          location.href="../login.html";
      }

    return Promise.reject(error)
  }
)
